// August Eurich    lab06   Part B    10/17/18
// This lab will ask the user to input an integer between 1 and 10,
// and it will output a pyramid of numbers with that many rows.
import java.util.Scanner;

public class PatternB{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please input an integer between 1 and 10:");
    boolean correctInt = false;
    int numRows = 0;
    correctInt = myScanner.hasNextInt();
    while (!correctInt){
      myScanner.next();
      System.out.println("That was not an integer, please input an integer between 1 and 10:");
      correctInt = myScanner.hasNextInt();
    } if (correctInt){
      numRows = myScanner.nextInt();  }
    
   while (numRows < 1 || numRows > 10){
      myScanner.next();
      System.out.println("That integer was outside the range of 1 to 10, please retry:");
      numRows = myScanner.nextInt();  }
    
    for (int counter2 = numRows; counter2 > 0; counter2--){
      for (int counter = 1; counter <= counter2; counter++){
        System.out.print(counter + " ");
      }
      System.out.println("");
    } // This spot marks the end of the for loops.
   
    } 
  }
