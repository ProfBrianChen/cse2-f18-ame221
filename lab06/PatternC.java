// August Eurich    lab06   Part C    10/18/18
// This lab will ask the user to input an integer between 1 and 10,
// and it will output a pyramid of numbers with that many rows.
import java.util.Scanner;

public class PatternC{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please input an integer between 1 and 10:");
    boolean correctInt = false;
    int numRows = 0;
    correctInt = myScanner.hasNextInt();
    while (!correctInt){
      myScanner.next();
      System.out.println("That was not an integer, please input an integer between 1 and 10:");
      correctInt = myScanner.hasNextInt();
    } if (correctInt){
      numRows = myScanner.nextInt();  }
    
   while (numRows < 1 || numRows > 10){
      myScanner.next();
      System.out.println("That integer was outside the range of 1 to 10, please retry:");
      numRows = myScanner.nextInt();  }
    
    int counter3 = 0;
    
    for (int counter = 1; counter <= numRows; counter++){
      counter3 = numRows - counter;
      for (int counter2 = counter; counter2 > 0; counter2--){
        while (counter3 > 0){
          System.out.print(" ");
          counter3--;
        }
        System.out.print(counter2);
      }
      System.out.println("");
    } // This spot marks the end of the for loops.
  }
}