// August Eurich    CSE 002   lab03-Check
// This program will gather user input regarding the
// cost of the check, the percentage tip, and the
// number of people splitting the check. It will then
// output how much each person should pay.


// Start by importing the Scanner
import java.util.Scanner;

public class Check{
  // main method required for every Java program
  public static void main(String[] args){
    // Create the Scanner
    Scanner myScanner = new Scanner( System.in );
    
    // Prompt the user to input the cost of the check:
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    // Accept the user's input:
    double checkCost = myScanner.nextDouble();
    
    // Prompt the user to input the percentage tip:
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    
    tipPercent /= 100;    // We want to convert the percentage into a decimal value.
   
    // Prompt the user to input the amount of people at dinner:
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    // Now we print the outputs:
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int) costPerPerson;
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
  }
}