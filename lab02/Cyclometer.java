// August Eurich    9/7/18    CSE 002
// Cyclometer measures the number of seconds elapsed and
// the number of rotations of the front wheel.
public class Cyclometer {
  // main method required for every Java program
  public static void main(String[] args) {
    int secsTrip1 = 480; // Seconds in the first trip is equal to 480
    int secsTrip2 = 3220; // Seconds in the second trip is equal to 3220
    int countsTrip1 = 1561; // Front wheel rotated 1561 times in first trip
    int countsTrip2 = 9037; // Front wheel rotated 9037 times in second trip
    
    // Now we have some useful constants...
    double wheelDiameter = 27.0; // Diameter of the bike wheel is 27 inches
    double PI = 3.14159; // Setting PI equal to a fairly precise value of pi
    double feetPerMile = 5280.0; // There are 5280 feet in a mile
    double inchesPerFoot = 12.0; // There are 12 inches in a foot
    double secondsPerMinute = 60.0; // There are 60 seconds in a minute
    
    // These are the end result variables:
    double distanceTrip1;
    double distanceTrip2;
    double totalDistance;
    
    // Now we start printing the results:
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) 
                      + " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) 
                      + " minutes and had " + countsTrip2 + " counts.");
    
    // Now we run the calculations, finding the distance in miles of each
    // trip and then print them to the screen.
    distanceTrip1 = (countsTrip1 * wheelDiameter * PI)/(inchesPerFoot * feetPerMile);
    distanceTrip2 = (countsTrip2 * wheelDiameter * PI)/(inchesPerFoot * feetPerMile);
    totalDistance = distanceTrip1 + distanceTrip2;
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
    
  }
}