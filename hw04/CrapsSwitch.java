// August Eurich    CSE 002   hw04    Part 2
// This program will ask the user if they want to randomly roll
// two dice or if they want to provide two numbers on their own.
// Given what they input or roll, this program will then output
// the slang name for the two dice.
import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter 1 if you want me to roll a pair of dice, or 2 if you want to enter your own values.");
    
    int userIn = myScanner.nextInt();
    int firstDie = 0;
    int secondDie = 0;
    int diceSum = 0;
    
    switch (userIn){
      case 1: firstDie = (int)(Math.random()*6) + 1;
              secondDie = (int)(Math.random()*6) + 1;
              diceSum = (firstDie*10) + secondDie;
        break;
      case 2: System.out.println("Please separately enter two integers between 1 and 6.");
              firstDie = myScanner.nextInt();
              secondDie = myScanner.nextInt();
              diceSum = (firstDie*10) + secondDie;
        break;
    }
    
     switch (diceSum){
       case 11: System.out.println("You rolled Snake Eyes.");
         break;
       case 12: System.out.println("You rolled Ace Deuce.");
         break;
       case 13: System.out.println("You rolled Easy Four.");
         break;
       case 14: System.out.println("You rolled Fever Five.");
         break;
       case 15: System.out.println("You rolled Easy Six.");
         break;
       case 16: System.out.println("You rolled Seven Out.");
         break;
       case 21: System.out.println("You rolled Ace Deuce.");
         break;
       case 22: System.out.println("You rolled Hard Four.");
         break;
       case 23: System.out.println("You rolled Fever Five.");
         break;
       case 24: System.out.println("You rolled Easy Six.");
         break;
       case 25: System.out.println("You rolled Seven Out.");
         break;
       case 26: System.out.println("You rolled Easy Eight.");
         break;
       case 31: System.out.println("You rolled Easy Four.");
         break;
       case 32: System.out.println("You rolled Fever Five.");
         break;
       case 33: System.out.println("You rolled Hard Six.");
         break;
       case 34: System.out.println("You rolled Seven Out.");
         break;
       case 35: System.out.println("You rolled Easy Eight.");
         break;
       case 36: System.out.println("You rolled Nine.");
         break;
       case 41: System.out.println("You rolled Fever Five.");
         break;
       case 42: System.out.println("You rolled Easy Six.");
         break;
       case 43: System.out.println("You rolled Seven Out.");
         break;
       case 44: System.out.println("You rolled Hard Eight.");
         break;
       case 45: System.out.println("You rolled Nine.");
         break;
       case 46: System.out.println("You rolled Easy Ten.");
         break;
       case 51: System.out.println("You rolled Easy Six.");
         break;
       case 52: System.out.println("You rolled Seven Out.");
         break;
       case 53: System.out.println("You rolled Easy Eight.");
         break;
       case 54: System.out.println("You rolled Nine.");
         break;
       case 55: System.out.println("You rolled Hard Ten.");
         break;
       case 56: System.out.println("You rolled Yo-leven.");
         break;
       case 61: System.out.println("You rolled Seven Out.");
         break;
       case 62: System.out.println("You rolled Easy Eight.");
         break;
       case 63: System.out.println("You rolled Nine.");
         break;
       case 64: System.out.println("You rolled Easy Ten.");
         break;
       case 65: System.out.println("You rolled Yo-leven.");
         break;
       case 66: System.out.println("You rolled Boxcars.");
         break;
     }
    System.out.println("Thank you for playing Craps today.");
  }
}