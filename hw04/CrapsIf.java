// August Eurich    CSE 002   hw04 Part 1
// This program will ask the user if they want to randomly roll
// two dice or if they want to provide two numbers on their own.
// Given what they input or roll, this program will then output
// the slang name for the two dice.
import java.util.Scanner;

public class CrapsIf{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Please enter 1 if you want me to roll a pair of dice, or 2 if you want to enter your own values.");
    
    int Roll = myScanner.nextInt();
    int firstDie = 0;
    int secondDie = 0;
    int rollSum = 0;
    int firstNum = 0;
    int secondNum = 0;
    int newSum = 0;
    
    if (Roll == 1){
      firstDie = (int)(Math.random()*6) + 1;
      secondDie = (int)(Math.random()*6) + 1;
      rollSum = firstDie + secondDie;
    } if (rollSum == 2){
      System.out.println("You rolled Snake Eyes.");
    } else if (rollSum == 3){
      System.out.println("You rolled Ace Deuce.");
    } else if ((rollSum == 4) && (firstDie != 2)){
      System.out.println("You rolled Easy Four.");
    } else if ((rollSum == 4) && (firstDie == 2)){
      System.out.println("You rolled Hard Four.");
    } else if (rollSum == 5){
      System.out.println("You rolled Fever Five.");
    } else if ((rollSum == 6) && (firstDie != 3)){
      System.out.println("You rolled Easy Six.");
    } else if ((rollSum == 6) && (firstDie == 3)){
      System.out.println("You rolled Hard Six.");
    } else if (rollSum == 7){
      System.out.println("You rolled Seven Out.");
    } else if ((rollSum == 8) && (firstDie != 4)){
      System.out.println("You rolled Easy Eight.");
    } else if ((rollSum == 8) && (firstDie == 4)){
      System.out.println("You rolled Hard Eight.");
    } else if (rollSum == 9){
      System.out.println("You rolled Nine.");
    } else if ((rollSum == 10) && (firstDie != 5)){
      System.out.println("You rolled Easy Ten.");
    } else if ((rollSum == 10) && (firstDie == 5)){
      System.out.println("You rolled Hard Ten.");
    } else if (rollSum == 11){
      System.out.println("You rolled Yo-leven.");
    } else if (rollSum == 12){
      System.out.println("You rolled Boxcars.");
    } if (Roll == 2){
      System.out.println("Please separately enter two integers between 1 and 6:");
      firstNum = myScanner.nextInt();
      secondNum = myScanner.nextInt();
      newSum = firstNum + secondNum;
    } if (newSum == 2){
      System.out.println("You rolled Snake Eyes.");
    } else if (newSum == 3){
      System.out.println("You rolled Ace Deuce.");
    } else if ((newSum == 4) && (firstNum != 2)){
      System.out.println("You rolled Easy Four.");
    } else if ((newSum == 4) && (firstNum == 2)){
      System.out.println("You rolled Hard Four.");
    } else if (newSum == 5){
      System.out.println("You rolled Fever Five.");
    } else if ((newSum == 6) && (firstNum != 3)){
      System.out.println("You rolled Easy Six.");
    } else if ((newSum == 6) && (firstNum == 3)){
      System.out.println("You rolled Hard Six.");
    } else if (newSum == 7){
      System.out.println("You rolled Seven Out.");
    } else if ((newSum == 8) && (firstNum != 4)){
      System.out.println("You rolled Easy Eight.");
    } else if ((newSum == 8) && (firstNum == 4)){
      System.out.println("You rolled Hard Eight.");
    } else if (newSum == 9){
      System.out.println("You rolled Nine.");
    } else if ((newSum == 10) && (firstNum != 5)){
      System.out.println("You rolled Easy Ten.");
    } else if ((newSum == 10) && (firstNum == 5)){
      System.out.println("You rolled Hard Ten.");
    } else if (newSum == 11){
      System.out.println("You rolled Yo-leven.");
    } else if (newSum == 12){
      System.out.println("You rolled Boxcars.");
    } else if (newSum > 12){
      System.out.println("Please make sure both integers are between 1 and 6.");
    }         
      System.out.println("Thank you for playing craps today.");   
  }
  } 