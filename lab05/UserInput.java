// August Eurich      CSE 002   Lab 5
// This program will ask the user to input various information
// about a class they are taking, and it will verify that this
// information is of the correct type, and print the information
// to the screen.
import java.util.Scanner;

public class UserInput{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    int courseNum = 0;
    String departmentName = "";
    int weeklyNum = 0;
    String courseTime = "";
    String profName = "";
    int numStudents = 0;
    
    System.out.println("Please enter your course number as an integer:");
    boolean correctInt = myScanner.hasNextInt();
    while (!correctInt){
      myScanner.next();
      System.out.println("That was not an integer. Please input an integer for your course number:");
      correctInt = myScanner.hasNextInt();
    } if (correctInt){
      courseNum = myScanner.nextInt();
    } 
    
    System.out.println("Please enter the department name:");
    departmentName = myScanner.next();
    
    System.out.println("Please enter the number of times per week the course meets:");
    boolean correctInt2 = myScanner.hasNextInt();
    while (!correctInt2){
      myScanner.next();
      System.out.println("That was not an integer. Please input an integer for the number of weekly course meetings:");
      correctInt2 = myScanner.hasNextInt();
    } if (correctInt2){
      weeklyNum = myScanner.nextInt();
    } if (weeklyNum > 7 || weeklyNum < 1){
      System.out.println("Class must meet at least once and at most seven times. Please enter a valid integer:");
      correctInt2 = false;
      weeklyNum = myScanner.nextInt();
    } else {
      weeklyNum = myScanner.nextInt();
    }
    
    System.out.println("Please enter the time the class starts at:");
    courseTime = myScanner.next();
    
    System.out.println("Please enter the professor's name:");
    profName = myScanner.next();
    
    System.out.println("Please enter the number of students in the class as an integer:");
    boolean correctInt3 = myScanner.hasNextInt();
    while (!correctInt3){
      myScanner.next();
      System.out.println("That was not an integer. Please input an integer for the number of students in class:");
      correctInt3 = myScanner.hasNextInt();
    } numStudents = myScanner.nextInt();
    
    System.out.println("Your class data is as follows:");
    System.out.println("Course Number: " + courseNum);
    System.out.println("Department Name: " + departmentName);
    System.out.println("Weekly Meetings: " + weeklyNum);
    System.out.println("Start Time: " + courseTime);
    System.out.println("Professor Name: " + profName);
    System.out.println("Students: " + numStudents);
  }
}