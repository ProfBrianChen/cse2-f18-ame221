// August Eurich    hw06    Encrypted X    10/19/18
// This program will ask the user for an integer between 0 and 100,
// and will then output a square of that size with an encrypted X.

import java.util.Scanner;
  
  public class EncryptedX{
  public static void main(String[] args){
    
    Scanner in = new Scanner(System.in);
    
    int square = 0;
    
    // Now we begin the process of getting the user input and testing it.
    System.out.println("Please input an integer between 0 and 100:");
    boolean integer = in.hasNextInt();
    while (!integer) {
      in.next();
      System.out.println("That was not an integer. Please input and integer between 0 and 100:");
      integer = in.hasNextInt();    }
    if (integer) {
      square = in.nextInt() - 1;    }
    
    // Now we have to see if the integer is between 0 and 100.
    while (square > 100 || square < 0) {
      System.out.println("The integer needs to be between 0 and 100. Please retry:");
      square = in.nextInt();    }
    
    // Here are the for loops, and we use an if-else sequence to decide if each character
    // is a space or an asterisk.
    for (int counter = 0; counter <= square; counter++) {
      for (int counter2 = 0; counter2 <= square; counter2++) {
        if (counter2 == counter || counter2 == (square-counter)) {
          System.out.print(" ");    }
        else { System.out.print("*");    }
        
        }
      System.out.println("");    // Here we have to reset each line for the outer for loop.
  }
    // End of program.
  }
  }