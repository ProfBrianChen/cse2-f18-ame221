// August Eurich    CSE 002   lab04
// This program will generate a random card out of a
// 52 card deck and tell the user which card they got.
public class CardGenerator{
  public static void main(String[] args){
    
    // Start by declaring the random number variable:
    int randomNum;
    randomNum = (int)(Math.random()*52) + 1;
    
    // Now we declare the suit and card number variables, created using mods:
    int cardNum;
    cardNum = randomNum % 13;
    String cardNumName = "";
    int cardSuit;
    cardSuit = randomNum % 4;
    String cardSuitName = "";
    
    // Now we use if statements to assign proper outputs for the card value:    
    if (cardNum == 0){
      cardNumName = "King";
    } else if (cardNum == 11){
      cardNumName = "Jack";
    } else if (cardNum == 12){
      cardNumName = "Queen";
    } else if (cardNum == 1){
      cardNumName = "Ace";
    } else if (cardNum == 2){
      cardNumName = "2";
    } else if (cardNum == 3){
      cardNumName = "3";
    } else if (cardNum == 4){
      cardNumName = "4";
    } else if (cardNum == 5){
      cardNumName = "5";
    } else if (cardNum == 6){
      cardNumName = "6";
    } else if (cardNum == 7){
      cardNumName = "7";
    } else if (cardNum == 8){
      cardNumName = "8";
    } else if (cardNum == 9){
      cardNumName = "9";
    } else if (cardNum == 10){
      cardNumName = "10";
    }
    
    // Now I will assign proper outputs for the card suit:
    switch (cardSuit){
      case 0: cardSuitName = "Hearts";
        break;
      case 1: cardSuitName = "Diamonds";
        break;
      case 2: cardSuitName = "Clubs";
        break;
      case 3: cardSuitName = "Spades";  
    }
    
    // Now each value between 1 and 52 has been assigned a value and suit.
    // I will now tell the user what card they drew:
    System.out.println("You picked the " + cardNumName + " of " + cardSuitName + ".");
  }
}