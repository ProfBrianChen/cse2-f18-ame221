// August Eurich    hw03    Part2
// This program will accept user input of 2 values.
// These values will be the length of a side of a 
// square pyramid and the height of the pyramid.
// This program will output the volume of said pyramid.
import java.util.Scanner;

public class Pyramid{
  public static void main(String[] args){
  Scanner myScanner = new Scanner( System.in );
  // I first have to prompt the user to input the side length:
  System.out.print("Enter the length of a side of the square pyramid in the form xx.xx: ");
  // Then I will accept the user's input:
  double sideLength = myScanner.nextDouble();
  // Then I ask the user for the height of the pyramid:
  System.out.print("Enter the height of the pyramid in the form xx.xx: ");
  // And then I accept the new input:
  double pyramidHeight = myScanner.nextDouble();
  
  // Now we have to do the calculations and get the volume of the pyramid.
  // I will take the length of a side and square that value, and then multiply it
  // by the height and finally divide by 3. This will give us the volume
  // of the square pyramid.
  
  double areaOfBase = (sideLength * sideLength);
  double totalVolume = (areaOfBase * pyramidHeight) / 3;
  
  System.out.println("The volume of your pyramid is: " + totalVolume + ".");
  System.out.println("Peace.");
  }
}