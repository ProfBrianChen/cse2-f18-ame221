// August Eurich    hw03    Part1
// This program will take user input, namely the number
// of acres of land affected by rainfall, and how many 
// inches of rain there was, and change these values into
// cubic miles of rainfall.
import java.util.Scanner;

public class Convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in );
    // First I will prompt the user to input the number of land acres:
    System.out.print("Enter the affected area in acres in the form xx.xx: ");
    // Now I accept his or her input:
    double rainAcres = myScanner.nextDouble();
    // Now I prompt the user to input the number of inches of rainfall:
    System.out.print("Enter rainfall in the affected region in inches in the form xx.xx: ");
    // And then accept the next input:
    double rainInches = myScanner.nextDouble();
    
    // Now we have to do the conversion and output the cubic miles of rainfall:
    // There are 640 acres in a square mile, so if we divide the acres by 640,
    // that will give us the area of the bottom of our "cube" of rainfall.
    // Then, we will take the rain inches and divide them by 12 and then divide that
    // number by 5280 to get the height of our "cube" of rain. We will then multiply
    // those 2 values together and get the cubic miles of rainfall and output it.
    
    double squareMilesOfLand = (rainAcres / 640);
    double rainMiles = (rainInches / 12) / 5280;
    double cubicMilesOfRain = (squareMilesOfLand * rainMiles);
      
      System.out.println("There were " + cubicMilesOfRain + " cubic miles of rainfall.");
  }
}