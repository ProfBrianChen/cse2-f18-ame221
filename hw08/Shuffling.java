// August Eurich    Hw08    Shuffling
// This program will present a deck of cards, shuffle them,
// and print out the shuffled version. Finally it will print
// out a single hand of five random cards.

import java.util.*;

public class Shuffling{
  
 // First I will create the printArray method:
 public static void printArray(String[] arrays){
   for(int i = 0; i < arrays.length; i++){
     System.out.print(arrays[i] + " ");
   }
 }
 
 // Now I create the shuffle method, which swaps a random card
 // with the first card 200 times and prints the shuffled deck:
 public static void shuffle(String[] arrays){
   System.out.println("Shuffled Deck:");
   String placeholder = "a";
   Random rand = new Random();
   int X = 0;
   for(int k = 0; k < 200; k++){
     X = rand.nextInt(52);
     placeholder = arrays[X];
     arrays[X] = arrays[0];
     arrays[0] = placeholder;
   }
   for(int i = 0; i < arrays.length; i++){
     System.out.print(arrays[i] + " ");
   }
   System.out.println("");
 }
 
 // Now I create the getHand method which returns a hand of 5 cards:
 public static String[] getHand(String[] arrays, int index, int numCards){
   String[] hand = new String[5];
   for(int y = 0; y < numCards; y++){
     hand[y] = arrays[index];
     index--;
   }
   return hand;
 }
 
 // Now I put in the main method which calls the above methods
 // in order to execute the program:
 public static void main(String[] args){
   Scanner scan = new Scanner(System.in);
   String[] suitNames = {"C","H","S","D"};
   String[] rankNames = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
   String[] cards = new String[52];
   String[] hand = new String[5];
   int numCards = 5;
   int again = 1;
   int index = 51;
   for(int i = 0; i < 52; i++){
     cards[i] = rankNames[i%13] + suitNames[i%4];
   }
   printArray(cards);
   shuffle(cards);
   while(again == 1){
     hand = getHand(cards, index, numCards);
     printArray(hand);
     index = index - numCards;
     System.out.println("");
     System.out.println("Enter a 1 if you want another hand drawn:");
     again = scan.nextInt();
   }
 }
   
}