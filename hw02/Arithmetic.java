// August Eurich    hw02    Arithmetic Calculations
public class Arithmetic{
  public static void main(String[] args){
    
    // Below we start with declaring and assigning variables:
    
    
    int numPants; // Number of pairs of pants
    int numShirts; // Number of sweatshirts
    int numBelts; // Number of belts
    numPants = 3;
    numShirts = 2;
    numBelts = 1;
    
    double pantsPrice; // Cost per pair of pants
    double shirtPrice; // Cost per shirt
    double beltCost; // Cost per belt
    pantsPrice = 34.98;
    shirtPrice = 24.99;
    beltCost = 33.99;
    
    double paSalesTax; // The tax rate
    paSalesTax = 0.06;
    
    // Now we start calculating some totals:
    
    double totalCostOfPants; // Total cost of the pants
    totalCostOfPants = numPants * pantsPrice;
    double totalCostOfShirts; // Total cost of the shirts
    totalCostOfShirts = numShirts * shirtPrice;
    double totalCostOfBelts; // Total cost of the belts
    totalCostOfBelts = numBelts * beltCost;
    
    double totalTaxOnPants; // Total tax charged to the pants
    totalTaxOnPants = totalCostOfPants * paSalesTax;
    totalTaxOnPants = Math.round(totalTaxOnPants * 100)/100.0; // We only want 2 decimal places
    double totalTaxOnShirts; // Total tax charged to the shirts
    totalTaxOnShirts = totalCostOfShirts * paSalesTax;
    totalTaxOnShirts = Math.round(totalTaxOnShirts * 100)/100.0; // We only want 2 decimal places
    double totalTaxOnBelts; // Total tax charged to the belts
    totalTaxOnBelts = totalCostOfBelts * paSalesTax;
    totalTaxOnBelts = Math.round(totalTaxOnBelts * 100)/100.0; // We only want 2 decimal places
    
    double totalCostOfPurchases; // Total cost of everything before tax
    totalCostOfPurchases = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    
    double totalSalesTax; // Total tax on all items
    totalSalesTax = totalTaxOnPants + totalTaxOnShirts + totalTaxOnBelts;
    
    double transactionTotal; // The final total for the transaction
    transactionTotal = totalCostOfPurchases + totalSalesTax;
    
    // Now we will display the values that need to be printed:
    
    System.out.println("Total cost of the pants is $" + totalCostOfPants);
    System.out.println("Total cost of the shirts is $" + totalCostOfShirts);
    System.out.println("Total cost of the belts is $" + totalCostOfBelts);
    System.out.println("Total tax on the pants is $" + totalTaxOnPants);
    System.out.println("Total tax on the shirts is $" + totalTaxOnShirts);
    System.out.println("Total tax on the belts is $" + totalTaxOnBelts);
    System.out.println("Total cost of items before tax is $" + totalCostOfPurchases);
    System.out.println("Total tax on all items is $" + totalSalesTax);
    System.out.println("The grand total for this transaction is $" + transactionTotal);
    System.out.println("Thank you for shopping with us today!");
    
  }
}