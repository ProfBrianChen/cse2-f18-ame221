// August Eurich    CSE 2   Hw09    Part 1
// This program will ask the user for 15 grades,
// and will then scramble and search the array
// using binary search, followed by linear search.

import java.util.*;

public class CSE2Linear{
  
  public static int[] randomize(int[] a){
    int placeholder = 0;
    Random rand = new Random();
    int X = 0;
    for(int k = 0; k < 50; k++){
     X = rand.nextInt(15);
     placeholder = a[X];
     a[X] = a[0];
     a[0] = placeholder;
  }  return a;
  }
  public static int linearSearch(int[] b, int bSize, int key){
      for (int z = 0; z < bSize; z++) {
         if (b[z] == key) {
            return z;
         }
      }      
      return -1;
  }

  public static int binarySearch(int[] c, int cSize, int key){
    int mid;
    int low;
    int high;
      
    low = 0;
    high = cSize - 1;

    while (high >= low) {
       mid = (high + low) / 2;
       if (c[mid] < key) {
          low = mid + 1;
       } 
       else if (c[mid] > key) {
          high = mid - 1;
       } 
       else {
          return mid;
         }
      }
      return -1;
  }
  public static void print(int[] d){
    for(int i = 0; i < d.length; i++){
     System.out.print(d[i] + " ");
  }  System.out.println("");
  }
  public static void main(String[] args){
    Scanner in = new Scanner(System.in);
    int[] Grades = new int[15];
    for(int x = 0; x < Grades.length; x++){
      System.out.println("Please enter a grade value:");
      while(!in.hasNextInt()){
        System.out.println("That was not an integer, please input an integer value:");
        in.next();
      } Grades[x] = in.nextInt();
      if(Grades[x] > 100 || Grades[x] < 0){
        System.out.println("That integer was out of the possible range, please enter an integer between 0 and 100:");
        in.next();
        Grades[x] = in.nextInt();
      }
      if(x > 0){
        if(Grades[x] < Grades[x-1]){
          System.out.println("That grade was less than the previous one. Please enter the grades in ascending order:");
          in.next();
          Grades[x] = in.nextInt();
        }
      }
    }  print(Grades);
    System.out.println("Please enter an integer value to be searched for:");
    int searchKey = in.nextInt();
    int spot = 0;
    spot = binarySearch(Grades, Grades.length, searchKey);
      if (spot == -1) {
         System.out.println(spot + " was not found.");
      } 
      else {
         System.out.println(searchKey + " Was found at index " + spot + ".");
  }
    Grades = randomize(Grades);
    print(Grades);
    System.out.println("Please enter an integer value to be searched for:");
    int searchKey2 = in.nextInt();
    int spot2 = 0;  
      spot2 = linearSearch(Grades, Grades.length, searchKey2);
      
      if (spot2 == -1) {
         System.out.println(searchKey2 + " was not found.");
      } 
      else {
         System.out.println(searchKey2 + " Was found after " + (spot2 + 1) + " iterations.");
}
}
}