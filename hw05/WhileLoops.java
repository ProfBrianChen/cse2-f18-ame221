// August Eurich    hw05    While Loops
// This program will ask the user for an integer, and will
// generate that many poker hands. Out of these hands, this
// program will calculate how many have one pair, two pairs,
// three-of-a-kind, and four-of-a-kind. Then, the calculated
// probabilities of each hand will be displayed.
import java.util.Scanner;

public class WhileLoops{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    int numLoops = 0;
    int counter = 0;
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    int card1num = 0;
    int card2num = 0;
    int card3num = 0;
    int card4num = 0;
    int card5num = 0;
    int numPairs = 0;
    int num2Pairs = 0;
    int numTriples = 0;
    int numQuads = 0;
    
    System.out.println("Please enter an integer, preferably larger than 100, to generate that many poker hands:");
    boolean correctInt = myScanner.hasNextInt();
    while (!correctInt){
      myScanner.next();
      System.out.println("That was not an integer, please enter an integer:");
      correctInt = myScanner.hasNextInt();
    } if (correctInt){
      numLoops = myScanner.nextInt();
    }
    
    while (counter < numLoops){
      counter = counter + 1;
      card1 = (int)(Math.random()*52) + 1;
      card2 = (int)(Math.random()*52) + 1;
      while (card1 == card2){
        card2 = (int)(Math.random()*52) + 1;  }
      card3 = (int)(Math.random()*52) + 1;
      while (card3 == card1 || card3 == card2){
        card3 = (int)(Math.random()*52) + 1;  }
      card4 = (int)(Math.random()*52) + 1;
      while (card4 == card3 || card4 == card2 || card4 == card1){
        card4 = (int)(Math.random()*52) + 1;  }
      card5 = (int)(Math.random()*52) + 1;
      while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1){
        card5 = (int)(Math.random()*52) + 1;  }
      card1num = card1 % 13;
      card2num = card2 % 13;
      card3num = card3 % 13;
      card4num = card4 % 13;
      card5num = card5 % 13;
      if (card1num == card2num && card3num != card4num && card3num != card5num && card4num != card5num && card2num != card3num && card2num != card4num && card2num != card5num){
        numPairs = numPairs + 1;  }
      else if (card1num == card3num && card2num != card4num && card2num != card5num && card4num != card5num && card3num != card2num && card3num != card4num && card3num != card5num){
        numPairs = numPairs + 1;  }
      else if (card1num == card4num && card2num != card3num && card2num != card5num && card3num != card5num && card4num != card2num && card4num != card3num && card4num != card5num){
        numPairs = numPairs + 1;  }
      else if (card1num == card5num && card2num != card3num && card2num != card4num && card3num != card4num && card5num != card2num && card5num != card3num && card5num != card4num){
        numPairs = numPairs + 1;  }
      else if (card2num == card3num && card1num != card4num && card1num != card5num && card4num != card5num && card3num != card1num && card3num != card4num && card3num != card5num){
        numPairs = numPairs + 1;  }
      else if (card2num == card4num && card1num != card3num && card1num != card5num && card3num != card5num && card4num != card1num && card4num != card3num && card4num != card5num){
        numPairs = numPairs + 1;  }
      else if (card2num == card5num && card1num != card3num && card1num != card4num && card3num != card4num){
        numPairs = numPairs + 1;  }
      else if (card3num == card4num && card1num != card2num && card1num != card5num && card2num != card5num){
        numPairs = numPairs + 1;  }
      else if (card3num == card5num && card1num != card2num && card1num != card4num && card2num != card4num){
        numPairs = numPairs + 1;  }
      else if (card4num == card5num && card1num != card2num && card1num != card3num && card2num != card3num){
        numPairs = numPairs + 1;  }
      else if (card1num == card2num && card1num == card3num && card2num == card3num && card4num != card5num){
        numTriples = numTriples + 1;  }
      else if (card1num == card2num && card1num == card4num && card2num == card4num && card3num != card5num){
        numTriples = numTriples + 1;  }
      else if (card1num == card2num && card1num == card5num && card2num == card5num && card3num != card4num){
        numTriples = numTriples + 1;  }
      else if (card1num == card3num && card1num == card4num && card3num == card4num && card2num != card5num){
        numTriples = numTriples + 1;  }
      else if (card1num == card3num && card1num == card5num && card3num == card5num && card2num != card4num){
        numTriples = numTriples + 1;  }
      else if (card1num == card4num && card1num == card5num && card4num == card5num && card2num != card3num){
        numTriples = numTriples + 1;  }
      else if (card2num == card3num && card2num == card4num && card3num == card4num && card1num != card5num){
        numTriples = numTriples + 1;  }
      else if (card2num == card3num && card2num == card5num && card3num == card5num && card1num != card4num){
        numTriples = numTriples + 1;  }
      else if (card2num == card4num && card2num == card5num && card4num == card5num && card1num != card3num){
        numTriples = numTriples + 1;  }
      else if (card3num == card4num && card3num == card5num && card4num == card5num && card1num != card2num){
        numTriples = numTriples + 1;  }
      else if (card1num == card2num && card1num == card3num && card1num == card4num && card1num != card5num){
        numQuads = numQuads + 1;  }
      else if (card1num == card2num && card1num == card3num && card1num == card5num && card1num != card4num){
        numQuads = numQuads + 1;  }
      else if (card1num == card2num && card1num == card4num && card1num == card5num && card1num != card3num){
        numQuads = numQuads + 1;  }
      else if (card1num == card3num && card1num == card4num && card1num == card5num && card1num != card2num){
        numQuads = numQuads + 1;  }
      else if (card2num == card3num && card2num == card4num && card2num == card5num && card2num != card1num){
        numQuads = numQuads + 1;  }
      else if (card1num == card2num && card3num == card4num && card1num != card5num && card3num != card5num && card1num != card3num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card2num && card3num == card5num && card1num != card4num && card3num != card4num && card1num != card3num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card2num && card4num == card5num && card1num != card3num && card4num != card3num && card1num != card4num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card3num && card2num == card4num && card1num != card5num && card2num != card5num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card3num && card2num == card5num && card1num != card4num && card2num != card4num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card3num && card4num == card5num && card1num != card2num && card4num != card2num && card1num != card4num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card4num && card2num == card3num && card1num != card5num && card2num != card5num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card4num && card2num == card5num && card1num != card3num && card2num != card3num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card4num && card3num == card5num && card1num != card2num && card3num != card2num && card1num != card3num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card5num && card2num == card3num && card1num != card4num && card2num != card4num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card5num && card2num == card4num && card1num != card3num && card2num != card3num && card1num != card2num){
        num2Pairs = num2Pairs + 1;  }
      else if (card1num == card5num && card3num == card4num && card1num != card2num && card3num != card2num && card1num != card3num){
        num2Pairs = num2Pairs + 1;  }
      else if (card2num == card3num && card4num == card5num && card2num != card1num && card4num != card1num && card2num != card4num){
        num2Pairs = num2Pairs + 1;  }
      else if (card2num == card4num && card3num == card5num && card2num != card1num && card3num != card1num && card2num != card3num){
        num2Pairs = num2Pairs + 1;  }
      else if (card2num == card5num && card3num == card4num && card2num != card1num && card3num != card1num && card2num != card3num){
        num2Pairs = num2Pairs + 1;  }
    }
    double probPair = 0.0;
    double probTriple = 0.0;
    double probQuad = 0.0;
    double prob2Pair = 0.0;
    probPair = ((double)numPairs / numLoops);
    probTriple = ((double)numTriples / numLoops);
    probQuad = ((double)numQuads / numLoops);
    prob2Pair = ((double)num2Pairs / numLoops);
    
    System.out.println("Number of Hands Generated: " + numLoops);
    System.out.printf("Probability of a Pair: %.6f \n", probPair);
    System.out.printf("Probability of Three-of-a-Kind: %.6f \n", probTriple);
    System.out.printf("Probability of Four-of-a-Kind: %.6f \n", probQuad);
    System.out.printf("Probability of 2 Pairs: %.6f \n", prob2Pair);
    System.out.println("Thanks for playing Poker today!");
  }
}