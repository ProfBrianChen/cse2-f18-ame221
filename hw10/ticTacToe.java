// August Eurich    Hw10    Tic-Tac-Toe
// This program will allow two users to enjoy a game
// of Tic-Tac-Toe.

import java.util.*;
  
public class ticTacToe{
  
  // Method for printing a 2D array:
  public static void print(String[][] a){
    for(int i = 0; i < 3; i++){
      for(int j = 0; j < 3; j++){
        System.out.print(a[i][j] + " ");
      } System.out.println();
    }
  }
  
  // Method for changing a spot on the board to an X:
  public static String[][] swapX(String[][] b, String c){
    int counter = 0;
    if(c.equals("1")){
      b[0][0] = "X";
      counter++;   }
    if(c.equals("2")){
      b[0][1] = "X";
      counter++;   }
    if(c.equals("3")){
      b[0][2] = "X";
      counter++;   }   
    if(c.equals("4")){
      b[1][0] = "X";
      counter++;   }   
    if(c.equals("5")){
      b[1][1] = "X";
      counter++;   }   
    if(c.equals("6")){
      b[1][2] = "X";
      counter++;   }
    if(c.equals("7")){
      b[2][0] = "X";
      counter++;   }
    if(c.equals("8")){
      b[2][1] = "X";
      counter++;   }
    if(c.equals("9")){
      b[2][2] = "X";
      counter++;   }
    if(counter == 0){
      System.out.println("Player 1 entered an invalid board spot, so no new X was placed.");   }
  return b;
       }
  
  // Method for changing a spot on the board to an O:
  public static String[][] swapO(String[][] b, String c){
    int counter = 0;
    if(c.equals("1")){
      b[0][0] = "O";
      counter++;   }
    if(c.equals("2")){
      b[0][1] = "O";
      counter++;   }
    if(c.equals("3")){
      b[0][2] = "O";
      counter++;   }   
    if(c.equals("4")){
      b[1][0] = "O";
      counter++;   }   
    if(c.equals("5")){
      b[1][1] = "O";
      counter++;   }   
    if(c.equals("6")){
      b[1][2] = "O";
      counter++;   }
    if(c.equals("7")){
      b[2][0] = "O";
      counter++;   }
    if(c.equals("8")){
      b[2][1] = "O";
      counter++;   }
    if(c.equals("9")){
      b[2][2] = "O";
      counter++;   }
    if(counter == 0){
      System.out.println("Player 2 entered an invalid board spot, so no new O was placed.");   }
  return b;
       }
  
  public static void main(String[] args){
    String[][] board = {
      {"1", "2", "3"},
      {"4", "5", "6"},
      {"7", "8", "9"}  };
    Scanner myScanner = new Scanner(System.in);
    String move = "0";
    boolean game = true;
    int turn = 1;
    print(board);
    System.out.println("This is your Tic-Tac-Toe board.");
      while(game){
        if((turn % 2) == 1){
          System.out.println("Player 1 enter a number to place your X there.");
          move = myScanner.nextLine();
      } if((turn % 2) == 0){
          System.out.println("Player 2 enter a number to place your O there.");
          move = myScanner.nextLine();
      }      
      if((turn % 2) == 1){
        board = swapX(board, move);
      }
      if((turn % 2) == 0){
        board = swapO(board, move);
      }
        turn++;
      if(board[0][0] == board[0][1] && board[0][0] == board[0][2] && board[0][1] == board[0][2]){
        game = false; }
      if(board[1][0] == board[1][1] && board[1][0] == board[1][2] && board[1][1] == board[1][2]){
        game = false; }
      if(board[2][0] == board[2][1] && board[2][0] == board[2][2] && board[2][1] == board[2][2]){
        game = false; }
      if(board[0][0] == board[1][0] && board[0][0] == board[2][0] && board[1][0] == board[2][0]){
        game = false; }
      if(board[0][1] == board[1][1] && board[0][1] == board[2][1] && board[1][1] == board[2][1]){
        game = false; }
      if(board[0][2] == board[1][2] && board[0][2] == board[2][2] && board[1][2] == board[2][2]){
        game = false; }
      if(board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[1][1] == board[2][2]){
        game = false; }
      if(board[2][0] == board[1][1] && board[2][0] == board[0][2] && board[1][1] == board[0][2]){
        game = false; }
      print(board);
      if(!game && (turn % 2) == 0){
        System.out.println("Game over! Congratulations to Player 1!"); 
      } if(!game && (turn % 2) == 1){
          System.out.println("Game over! Congratulations to Player 2!");    }
     
  }
}
  
  
}