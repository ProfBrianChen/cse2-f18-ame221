// August Eurich    CSE 002     11/9/2018
// Lab 08 ~ Arrays
// This program will generate a random array of 100
// numbers from 0 to 99, and then it tell the user
// how many times each number occured.

import java.util.Random;

public class TwoArrays{

  public static void main(String[] args){
    
    // Creating the first array:
    int[] firstArray = new int[100];
    for(int a = 0; a < firstArray.length; a++){
      firstArray[a] = (int)(Math.random()*100);
      System.out.print(firstArray[a] + " ");
    } System.out.println("");
    
    // Creating the second array to count the number of instances in the first one:
    int[] secondArray = new int[firstArray.length];
    for(int b = 0; b < firstArray.length; b++){
      secondArray[firstArray[b]]++;
    }
    // Now we will print out the number of instances of each int, if it occurs at least once:
    for(int c = 0; c < secondArray.length; c++){
      if (secondArray[c] > 0){
        System.out.println("The number " + c + " occurs " + secondArray[c] + " times.");
      }
    }
    
    
  }
}